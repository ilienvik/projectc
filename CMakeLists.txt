cmake_minimum_required(VERSION 3.26)
project(untitled1)
set(CMAKE_EXE_LINKER_FLAGS ${CMAKE_EXE_LINKER_FLAGS} "-static")
add_executable(untitled1 main.cpp
        ClassicMultiplication.cpp
        ClassicMultiplication.h
        BruteForce.cpp
        BruteForce.h
)


set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)
target_link_libraries(untitled1 Threads::Threads)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread")
