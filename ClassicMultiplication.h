//
// Created by orang on 12/13/2023.
//

#ifndef UNTITLED1_CLASSICMULTIPLICATION_H
#define UNTITLED1_CLASSICMULTIPLICATION_H

#include <vector>

/*Klasický algoritmus s blokovým násobením.*/

class ClassicMultiplication {
public:
    std::mutex mtx;
    template<typename T>
    bool checkValidity(const std::vector<std::vector<T>>&,const std::vector<std::vector<T>>&,const size_t &);

    template<typename T>
    std::vector<std::vector<T>> multiplyMatrices(const std::vector<std::vector<T>>&,const std::vector<std::vector<T>>&,const size_t&);

};


#include "ClassicMultiplication.cpp"

#endif //UNTITLED1_CLASSICMULTIPLICATION_H
