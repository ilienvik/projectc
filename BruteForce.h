//
// Created by orang on 12/13/2023.
//

#ifndef UNTITLED1_BRUTEFORCE_H
#define UNTITLED1_BRUTEFORCE_H

#include <vector>

/*Naivní metoda (brute force) */
class BruteForce {
public:

    template<typename T>
    bool checkValidity(const std::vector<std::vector<T>>&,const std::vector<std::vector<T>>&);

    template<typename T>
    std::vector<std::vector<T>> multiplyMatrices(const std::vector<std::vector<T>>&,const std::vector<std::vector<T>>&);

    template<typename T>
    std::vector<std::vector<T>>multiplyMatricesWithThreads(const std::vector<std::vector<T>> &leftMatrix, const std::vector<std::vector<T>> &rightMatrix);

};
#include "BruteForce.cpp"

#endif //UNTITLED1_BRUTEFORCE_H
