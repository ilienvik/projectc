//
// Created by orang on 12/13/2023.
//
#ifndef UNTITLED1_BRUTEFORCE_CPP
#define UNTITLED1_BRUTEFORCE_CPP

#include <stdexcept>
#include <mutex>
#include <thread>
#include <algorithm>
#include <numeric>
#include <iostream>
#include "BruteForce.h"


template<typename T>
bool BruteForce::checkValidity(const std::vector<std::vector<T>> &leftMatrix, const std::vector<std::vector<T>> &rightMatrix) {
   try{//basic rules check
    if((leftMatrix.empty()&&!rightMatrix.empty())
    ||(rightMatrix.empty()&&!leftMatrix.empty())){
        return false;
    }else if(!leftMatrix.empty()&&leftMatrix[0].size()!=rightMatrix.size()){return false;
    }
    //inconsistent row number check
    int previousLeftSize=0,previousRightSize=0;
    if(!leftMatrix.empty()) previousLeftSize=leftMatrix[0].size();
    if(!rightMatrix.empty()) previousRightSize=rightMatrix[0].size();
    for (int i = 0; i < leftMatrix.size(); i++) {
        for (int j = 0; j < rightMatrix.size(); j++) {
            if (
            leftMatrix[i].size()!=previousLeftSize||
            rightMatrix[j].size()!=previousRightSize) {
                return false;
            }
            previousLeftSize=leftMatrix[i].size();
            previousRightSize=rightMatrix[j].size();
        }
    }

    return true;
   }catch (std::exception&e)
   {
       std::cerr<<e.what();
       return false;
   }
}

template<typename T>
std::vector<std::vector<T>>
BruteForce::multiplyMatrices(const std::vector<std::vector<T>> & leftMatrix, const std::vector<std::vector<T>> & rightMatrix) {
    //check if both are empty
    if(leftMatrix.empty()&&rightMatrix.empty())return {};

    //checking if matrix is valid
    if(!checkValidity(leftMatrix,rightMatrix)){
        throw std::invalid_argument("Invalid matrix");
    }
    //create result matrix
    size_t leftRows = leftMatrix.size();
    size_t leftCols = leftMatrix[0].size();
    size_t rightCols =0;
    if(!rightMatrix.empty() && !rightMatrix[0].empty())rightCols=rightMatrix[0].size();

    std::vector<std::vector<T>> resultMatrix(leftRows, std::vector<T>(rightCols, 0));

    /*calculating product of the i row of the left matrix
    * and the j column of the right matrix + storing answer the result matrix.
    */
    for (size_t i = 0; i < leftRows; ++i) {
        for (size_t j = 0; j < rightCols; ++j) {

            resultMatrix[i][j] = std::inner_product(
                    leftMatrix[i].begin(), leftMatrix[i].end(),
                    rightMatrix.begin(), static_cast<T>(0),
                    std::plus<T>(), [j](const T& left, const std::vector<T>& rightRow) {
                        return left * rightRow[j];
                    }
            );
        }
    }

    return resultMatrix;
}

template<typename T>
std::vector<std::vector<T>> BruteForce::multiplyMatricesWithThreads(const std::vector<std::vector<T>> &leftMatrix, const std::vector<std::vector<T>> &rightMatrix) {
   try{
        if(leftMatrix.empty() && rightMatrix.empty()) {return {};}

        // Check validity
        if (!checkValidity(leftMatrix, rightMatrix)) {
            throw std::invalid_argument("Invalid matrix");
        }
        // Create result matrix
        size_t rows = leftMatrix.size();
        size_t cols = 0;
        if(!rightMatrix.empty() && !rightMatrix[0].empty())cols=rightMatrix[0].size();
        std::vector<std::vector<T>> resultMatrix(rows, std::vector<T>(cols, 0));
        std::mutex mtx;
       //same strategy as previous function + some additional checks
        auto multiplyThread = [&](size_t startRow, size_t endRow) {
           for (size_t i = startRow; i < endRow; ++i) {
               for (size_t j = 0; j < cols; ++j) {
                   T sum = std::inner_product(
                           leftMatrix[i].begin(), leftMatrix[i].end(),
                           rightMatrix.begin(), static_cast<T>(0),
                           std::plus<T>(), [j](const T& left, const std::vector<T>& rightRow) {
                               //case if index j is out of bounds for rightRow
                               if (j < rightRow.size()) {
                                   return left * rightRow[j];
                               } else {
                                   return static_cast<T>(0); //returning zero for missing elements
                               }
                           }
                   );
                   {
                       std::lock_guard<std::mutex> lock(mtx);
                       resultMatrix[i][j] = sum;
                   }
               }
           }
        };

       size_t numOfThreads = std::thread::hardware_concurrency();
        if (numOfThreads==0) {numOfThreads = 1;}

       //assigning number of rows thread should handle
       size_t numOfRowsPerThread = rows / numOfThreads;
       size_t additionalRows = rows % numOfThreads;
       std::vector<std::thread> threads;

       //distributing additional rows among the first threads
       size_t startRowIndex = 0;
       for (size_t i = 0; i < numOfThreads; ++i) {
           size_t endRowIndex = startRowIndex + numOfRowsPerThread + (i < additionalRows ? 1 : 0);
           threads.emplace_back(multiplyThread, startRowIndex, endRowIndex);
           startRowIndex = endRowIndex;
       }
       //joining
       for (auto &thread : threads) {
            thread.join();
       }

       return resultMatrix;
   }
   catch (std::exception&e)
   {
       std::cerr<<e.what();
       return {};
   }
}
#endif