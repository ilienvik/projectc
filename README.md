# Násobení matic

Toto není plná verze dokumentace, celé pdf najdete ve složce Dokument v Git.

## *Popis zadání*:
Násobení matic je jedna z výpočetně nejzajímavějších operací v lineární algebře.
V rámci tohoto úkolu byly vybrány dva algoritmy pro implementaci násobení matic: Brute-Force (naivní metoda) a Klasické blokové násobení. Kromě toho jsem vytvořila variantu algoritmu Brute-Force s využitím vícevláknového zpracování za účelem zvýšení výpočetního výkonu.


## *Popis implementace*:
- BruteForce

V implementaci algoritmu Brute-Force jsem provedla důkladnou kontrolu validity vstupních matic (function checkValidity()) a navrhla algoritmus pro násobení matic.
Implementovala jsem také verzi algoritmu s více vlákny, která paralelizuje výpočetní části a využívá mutex(std::mutex) k zajištění thread-safety při aktualizaci výsledné matice.

- Classic Multiplication

Ve třídě ClassicMultiplication jsem rovněž provedla kontrolu validity vstupních matic (function checkValidity()) a implementovala algoritmus pro klasické blokové násobení matic. Tento algoritmus využívá blokového zpracování pro optimalizaci výkonu.


## *Popis funkčnosti a ovládání aplikace*:
Při spuštění aplikace z konzole můžete zvolit jeden ze tří algoritmů: classic (klasické násobení s bloky), brute-force (hrubá síla) nebo brute-force2 (hrubá síla s vícevláknovým zpracováním).

Po výběru algoritmu můžete zvolit metodu generování matic.
Volby zahrnují :

-generování náhodných matic podle zadaných rozměrů

-použití výchozích matic z programu

-načtení matic ze specifických souborů

V případě volby této poslední možnosti je nutné mít soubory s maticemi již předem přidané do projektu. A pokud se přidaný soubor nenachází v hlavní složce, je třeba zapsat celou cestu (folder/file.txt).

Při spuštění s parametry z příkazového řádku můžete předat algoritmus a další volby přímo při spuštění.
Například:

./untitled1 classic --size 3 3 (spustí klasické násobení s bloky pro matice o rozměrech)

./untitled1 brute-force2 --from-file testsData/test1.txt testsData/test1.txt
(spustí brute-force násobení pro matice ze zadaných souborů)

 
## *Výsledky běhu programu a naměřené časy při porovnání jedno- a více- vláknové verze*:

Celé pdf včetně porovnání najdete ve složce Dokument v Git.


*[Link to Project Report (PDF)](https://yourprojectdocs.com)*