//
// Created by orang on 12/13/2023.
//
#ifndef UNTITLED1_MULTIPLICATION_CPP
#define UNTITLED1_MULTIPLICATION_CPP
#include <stdexcept>
#include <mutex>
#include <thread>
#include <iostream>
#include "ClassicMultiplication.h"


template<typename T>
bool ClassicMultiplication::checkValidity(const std::vector<std::vector<T>> &leftMatrix, const std::vector<std::vector<T>> &rightMatrix,const size_t &block) {
    try{
        //basic rules check
        if((leftMatrix.empty()&&!rightMatrix.empty())
           ||(rightMatrix.empty()&&!leftMatrix.empty())){
            return false;
        }else if(!leftMatrix.empty()&&leftMatrix[0].size()!=rightMatrix.size()){return false;
        }

        //inconsistent row number check
        int previousLeftSize=0,previousRightSize=0;
        if(!leftMatrix.empty()) previousLeftSize=leftMatrix[0].size();
        if(!rightMatrix.empty()) previousRightSize=rightMatrix[0].size();

        for (int i = 0; i < leftMatrix.size(); i++) {
            for (int j = 0; j < rightMatrix.size(); j++) {
                if (!leftMatrix.empty() && leftMatrix[i].size() != previousLeftSize) {
                    return false;
                }
                if (!rightMatrix.empty() && rightMatrix[0].size() != previousRightSize) {
                    return false;
                }
                previousLeftSize=leftMatrix[i].size();
                previousRightSize=rightMatrix[j].size();
            }
        }
        return true;

    }catch (std::exception&e)
    {
        std::cerr<<e.what();
        return false;
    }
}



template<typename T>
std::vector<std::vector<T>>
ClassicMultiplication::multiplyMatrices(const std::vector<std::vector<T>> &leftMatrix, const std::vector<std::vector<T>> &rightMatrix,const size_t & block) {
    if(leftMatrix.empty()&&rightMatrix.empty())return {};
    //checking matrix validity
   if(!checkValidity(leftMatrix,rightMatrix,block)){
        throw std::invalid_argument("Invalid matrix");
   }
    //create result matrix
    size_t leftMatrixRows = leftMatrix.size();
    size_t leftMatrixCols=0;
    if(!leftMatrix.empty() && !leftMatrix[0].empty())
        leftMatrixCols= leftMatrix[0].size();
    size_t rightMatrixCols =0;
    if(!rightMatrix.empty() && !rightMatrix[0].empty())
        rightMatrixCols=rightMatrix[0].size();

    std::vector<std::vector<T>> resultMatrix(leftMatrixRows, std::vector<T>(rightMatrixCols, 0));

    //multiplying using blocks
    for (size_t i = 0; i < leftMatrixRows; i += block) {
        for (size_t j = 0; j < rightMatrixCols; j += block) {
            for (size_t k = 0; k < leftMatrixCols; k += block) {
                //iterating through blocks and performing multiplication
                for (size_t ii = i; ii < std::min(i + block, leftMatrixRows); ++ii) {
                    for (size_t jj = j; jj < std::min(j + block, rightMatrixCols); ++jj) {
                        for (size_t kk = k; kk < std::min(k + block, leftMatrixCols); ++kk) {
                            //update result matrix
                            resultMatrix[ii][jj] += leftMatrix[ii][kk] * rightMatrix[kk][jj];
                        }
                    }
                }
            }
        }
    }
    return resultMatrix;
}

#endif