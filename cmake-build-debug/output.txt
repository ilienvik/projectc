./untitled1 : 
At line:1 char:1
+ ./untitled1 --help > output.txt 2>&1
+ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : NotSpecified: (:String) [], RemoteException
    + FullyQualifiedErrorId : NativeCommandError
 
End of program
Strart
Usage: C:\Users\orang\CLionProjects\untitled1\cmake-build-debug\untitled1.exe <chosenAlgorithm> [options]
Available algorithms:
 Classic (type 'classic') - Run classic matrix multiplication with blocks
 Brute-force (type 'brute-force') - Run brute-force matrix multiplication

Options:
  --help - Display this help message
  --size <width> <height> - Generate matrices by setting specific sizes of the first matrix
  --from-file - Use default matrices from program's files
