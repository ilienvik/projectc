#include <thread>
#include <iostream>
#include <mutex>
#include "ClassicMultiplication.h"
#include "BruteForce.h"
#include <random>
#include <vector>
#include <sstream>
#include <chrono>
#include <fstream>
#include <iomanip>
#include <iterator>
/*
 * contains functions needed for matrix generation
 */
template <typename T>
struct GeneratedMatrix {
    auto& getRng() {
        static std::minstd_rand rng{ std::random_device{}() };
        return rng;
    }
    double randomDouble(double from, double till) {
        auto& rng = getRng();
        return std::uniform_real_distribution<double>(from, till)(rng);
    }
    int randomInteger(int from, int till) {
        auto& rng = getRng();
        return std::uniform_int_distribution<int>(from, till)(rng);
    }
};
/*
 * generates two matrices of corresponding dimensions
 */
void generateMatrices(size_t desiredWidth, size_t desiredHeight, std::vector<std::vector<int>>& matrix1, std::vector<std::vector<int>>& matrix2) {

    GeneratedMatrix<int> genMatrix1;
    GeneratedMatrix<int> genMatrix2;

    matrix1.resize(desiredHeight, std::vector<int>(desiredWidth, 0));
    matrix2.resize(desiredWidth, std::vector<int>(desiredHeight, 0));

    for (size_t i = 0; i < desiredHeight; ++i) {
        for (size_t j = 0; j < desiredWidth; ++j) {
            matrix1[i][j] = genMatrix1.randomInteger(-1000, 1000);
            matrix2[j][i] = genMatrix2.randomInteger(-1000, 1000);
        }
    }
}
/*
 * calculates optimal block size for classic block multiplication
 */
size_t findAppropriateBlockSize(const std::vector<std::vector<int>>& matrix1, const std::vector<std::vector<int>>& matrix2) {
    size_t optimalBlockSize = 1;
    //in case empty matrices somehow get here
    if(matrix1.empty()&&matrix2.empty())return 0;
    if(matrix1.empty()||matrix2.empty())return 0;
    for (size_t block = 1; block <= std::min(matrix1.size(), matrix2[0].size()); ++block) {
        //checking if block evenly divides both matrix dimensions
        if (matrix1.size() % block == 0 && matrix2[0].size() % block == 0 &&
            matrix1[0].size() % block == 0 && matrix2.size() % block == 0 &&
            matrix1[0].size() >= block && matrix2.size() >= block) {
            optimalBlockSize = block;
        }
    }

    return optimalBlockSize;
}

template <typename T>
void printMatrix(const std::vector<std::vector<T>>& matrix) {
    if (matrix.empty()) {
        std::cout << "{}\n\n";
        return;
    }
    for (const auto& row : matrix) {
        for (const auto& el : row) {
            std::cout << std::setw(7) << el;
        }
        std::cout << "\n";
    }
    std::cout << "\n";
}
/*
 * runs multiplication algorithm based on the selected algorithm
 * and returns result matrix
 */
std::vector<std::vector<int>> runMultiplicationAlgorithm(const std::string& algorithm, std::vector<std::vector<int>>& matrix1, std::vector<std::vector<int>>& matrix2) {

    if (algorithm == "classic") {
        size_t block = findAppropriateBlockSize(matrix1, matrix2);
        ClassicMultiplication algorithmm;
        auto resultMatrix = algorithmm.multiplyMatrices(matrix1, matrix2,block);
        return resultMatrix;
    }else if (algorithm == "brute-force2") {
        BruteForce algorithm2;
        auto resultMatrix = algorithm2.multiplyMatricesWithThreads(matrix1, matrix2);
        return resultMatrix;

    } else if (algorithm == "brute-force") {
        BruteForce algorithm1;
        auto resultMatrix = algorithm1.multiplyMatrices(matrix1, matrix2);
        return resultMatrix;
    } else {
        std::cerr << "Error: Unknown algorithm '" << algorithm << "'\n";
        return {};
    }
}
/*
 * reads matrices from file and checks if they're compatible
 */
bool readMatricesFromFile(const std::string& file1, const std::string& file2, std::vector<std::vector<int>>& matrix1, std::vector<std::vector<int>>& matrix2) {
   try {
       //reading and checking dimensions
       std::ifstream fileStream1(file1), fileStream2(file2);
       if (!fileStream1.is_open() || !fileStream2.is_open()) {
           std::cerr<< "Unable to open one or both input files! Please make sure files exist and are added to cmake.\n";
           return false;
       }
       std::string line;

       while (std::getline(fileStream1, line)) {
           std::istringstream iss(line);
           matrix1.emplace_back(std::istream_iterator<int>(iss), std::istream_iterator<int>());
       }

       while (std::getline(fileStream2, line)) {
           std::istringstream iss(line);
           matrix2.emplace_back(std::istream_iterator<int>(iss), std::istream_iterator<int>());
       }

       if (matrix1.empty() || matrix2.empty() || matrix1[0].size() != matrix2.size()) {
           std::cerr << "Matrices have incompatible dimensions. Check the files please!\n";
           return false;
       }

       return true;
   }catch (std::exception&e){
       std::cerr<<e.what();
       return false;
   }
}

bool handleConsoleInput(std::string& chosenAlgorithm, size_t& desiredWidth, size_t& desiredHeight,
                        std::vector<std::vector<int>>& matrix1, std::vector<std::vector<int>>& matrix2) {
    try{
    std::cout << "Enter the chosen algorithm (classic/brute-force/brute-force2) or type: ";
    std::cin >> chosenAlgorithm;

    //showing help in case of incorrect input
    if (chosenAlgorithm != "classic" && chosenAlgorithm != "brute-force"&& chosenAlgorithm != "brute-force2") {
        std::cout<<"Unrecognised input!"<<std::endl;
        std::cout << "\n\nUsage: \n";
        std::cout << "When asked to 'Enter the chosen algorithm' you must pick between three algorithms :\n";
        std::cout << " Classic (type 'classic') Run classic matrix multiplication with blocks\n";
        std::cout << " Brute-force (type 'brute-force')- Run brute-force matrix multiplication\n";
        std::cout << " Brute-force2 (type 'brute-force2') Run brute-force matrix multiplication using threads\n";

        std::cout << "\nWhen asked to 'Choose an option' type 1 or 2 depending on desired method of generation:\n";
        std::cout << " Generate matrices by size(type '1') - generate matrices by setting specific sizes of the first matrix\n";
        std::cout << " Use default matrices from files(type '2') - use default matrices from program's files\n";
        std::cout << " Use default matrices from files(type '3') - use matrices from specific file paths.Make sure you've added necessary files.";
        return false;
    }

    std::cout << "Choose an option:\n";
    std::cout << "1. Generate matrices by size\n";
    std::cout << "2. Use default matrices from files\n";
    std::cout << "3. Use matrices from specific files\n";
    std::cout << "4. Show help\n";
    int option;
    std::cin >> option;

    if (option == 1) {
        try{
        //generate by size
        std::cout << "Enter the matrix size (width height): ";
        std::cin >> desiredWidth >> desiredHeight;
        if((desiredWidth==0&&desiredHeight!=0)||(desiredWidth!=0&&desiredHeight==0)){
            throw std::invalid_argument("Invalid matrix");
        }
        generateMatrices(desiredWidth, desiredHeight, matrix1, matrix2);}
        catch (std::exception&e){
            std::cerr<<"Invalid matrix dimensions. Width and height must be positive.";
            return false;
        }
    } else if (option == 2) {
        //declaring paths to default files
        std::string pathToFirst="firstMatrix.txt";
        std::string pathToSecond="secondMatrix.txt";

          if (!readMatricesFromFile(pathToFirst,pathToSecond,matrix1,matrix2)) {
            std::cerr << "Error: Something went wrong.\n";
            return false;
        }

    }else if(option==3){
        std::string pathToFirst, pathToSecond;

        std::cout << "Enter file names in format: firstMatrix.txt secondMatrix.txt\n";
        std::cin >> pathToFirst >> pathToSecond;

        if (!readMatricesFromFile(pathToFirst,pathToSecond,matrix1,matrix2)) {
            std::cerr << "Error: Something went wrong.\n";
            return false;
        }

    }else if(option==4){
        std::cout << "Usage: \n";
        std::cout << "When asked to 'Enter the chosen algorithm' you must pick between three algorithms :\n";
        std::cout << " Classic (type 'classic') Run classic matrix multiplication with blocks\n";
        std::cout << " Brute-force (type 'brute-force')- Run brute-force matrix multiplication\n";
        std::cout << " Brute-force2 (type 'brute-force2') - Run brute-force matrix multiplication using threads\n";


        std::cout << "When asked to 'Choose an option' type 1,2 or 3 depending on desired method of generation:\n";
        std::cout << "Generate matrices by size(type '1') - generate matrices by setting specific sizes of the first matrix\n";
        std::cout << "Use default matrices from files(type '2') - use default matrices from program's files\n";
        std::cout << "Use matrices from specific files(type '3') - use matrices from specific file paths.Make sure you've added necessary files.\n";
        return false;
    } else {
        std::cerr << "Invalid option selected!\n";
        return false;
    }
    return true;
    }catch (std::exception&e){
        std::cerr<<e.what();
        return false;
    }
}

bool handleCommandLineArguments(int argc, char* argv[], std::string& chosenAlgorithm,
                                size_t& desiredWidth, size_t& desiredHeight,
                                std::vector<std::vector<int>>& matrix1, std::vector<std::vector<int>>& matrix2) {
    if (argc <1) return false;
    try{
        //displaying help
        if (argc == 2 && std::string(argv[1]) == "--help") {
            std::cout << "\nUsage: " << argv[0] << " <chosenAlgorithm> [options]"<<std::endl;
            std::cout << "Available algorithms:"<<std::endl;
            std::cout << " Classic (type 'classic') - Run classic matrix multiplication with blocks"<<std::endl;;
            std::cout << " Brute-force (type 'brute-force') - Run brute-force matrix multiplication"<<std::endl;;
            std::cout << " Brute-force2 (type 'brute-force2') - Run brute-force matrix multiplication using threads"<<std::endl;;

            std::cout << "\nOptions:"<<std::endl;;
            std::cout << "  --help - Display this help message"<<std::endl;;
            std::cout << "  --size <width> <height> - Generate matrices by setting specific sizes of the first matrix"<<std::endl;;
            std::cout << "  --from-file - Use default matrices from program's files"<<std::endl;
            std::cout << " --from-file matrix1.txt matrix2.txt - Use matrices from specific file paths. Make sure you've added necessary files!";
            return false;
        }
        chosenAlgorithm = argv[1];

        if (chosenAlgorithm != "classic" &&chosenAlgorithm!="brute-force2"&& chosenAlgorithm != "brute-force") {
            std::cerr << "Error: Unknown algorithm '" << chosenAlgorithm << "'. Use '--help' for instructions.\n";
            return false;
        }

        if (argc == 5 && std::string(argv[2]) == "--size") {
            try{
            desiredWidth = std::stoi(argv[3]);
            desiredHeight = std::stoi(argv[4]);
            if((desiredWidth==0&&desiredHeight!=0)||(desiredWidth!=0&&desiredHeight==0)){
                throw std::invalid_argument("Invalid matrix");
            }
            generateMatrices(desiredWidth, desiredHeight, matrix1, matrix2);
            }catch(std::exception&e){
                std::cerr<<"Invalid matrix dimensions. Width and height must be positive.";
                return false;
            }
        } else if (argc == 3 && std::string(argv[2]) == "--from-file") {
            //assigning default paths
            std::string pathToFirst="firstMatrix.txt";
            std::string pathToSecond="secondMatrix.txt";

            if (!readMatricesFromFile(pathToFirst,pathToSecond,matrix1,matrix2)) {
                std::cerr << "Error: Something went wrong.\n";
                return false;
            }

        } else if (argc == 5 && std::string(argv[2]) == "--from-file") {
            std::string pathToFirst=std::string(argv[3]);
            std::string pathToSecond=std::string(argv[4]);

            if (!readMatricesFromFile(pathToFirst,pathToSecond,matrix1,matrix2)) {
                std::cerr << "Error: Something went wrong.\n";
                return false;
            }
        }
        else {
            std::cerr << "Error: Invalid command line arguments. Use '--help' for instructions.\n";
            return false;
        }
        return true;
    }
    catch (std::exception&e)
    {
        std::cerr<<e.what();
        return false;
    }
}

int main(int argc, char* argv[]) {
    std::string chosenAlgorithm;
    size_t desiredWidth = 0, desiredHeight = 0;
    std::vector<std::vector<int>> matrix1, matrix2,resultMatrix;

    if (argc < 2)
    {
        if(!handleConsoleInput(chosenAlgorithm, desiredWidth, desiredHeight, matrix1, matrix2)){
            std::cerr<<"\nEnd of program\n";
            return 0;}
    }
    else
    {
        if(!handleCommandLineArguments(argc, argv, chosenAlgorithm, desiredWidth, desiredHeight, matrix1, matrix2)){
            std::cerr<<"\nEnd of program\n";
            return 0;}
    }

    //I recommend commenting this part in case of time testing
    std::cout << "Matrix 1:\n";
    printMatrix(matrix1);
    std::cout << "Matrix 2:\n";
    printMatrix(matrix2);

    // start time
    auto start_naive = std::chrono::steady_clock::now();
    //running method
    resultMatrix=runMultiplicationAlgorithm(chosenAlgorithm, matrix1, matrix2);
    //end time
    auto end_naive = std::chrono::steady_clock::now();
    auto duration_naive = std::chrono::duration_cast<std::chrono::microseconds>(end_naive - start_naive);
    auto duration_seconds = std::chrono::duration_cast<std::chrono::seconds>(duration_naive);

    //I recommend commenting this part in case of time testing
    std::cout << "Result Matrix:\n";
    printMatrix(resultMatrix);

    //outputting time measurement
    std::cout << "Method ran for " << duration_seconds.count() << " seconds ( "<< duration_naive.count() << " microseconds).\n";
    std::cout << "\nEnd of program\n";
    return 0;
}